// import myCurrentLocation, { message, name, getGreeting } from './myModule'
// import ADD, { subtract } from './math'

// console.log(message, name, myCurrentLocation)
// console.log(getGreeting(name))

// console.log('1 + 2 =', ADD(1, 2))
// console.log('1 - 2 =', subtract(1, 2))

import { GraphQLServer } from 'graphql-yoga'

// Scalar types - String, Boolean, Int, Float, ID

// Demo post data
const posts = [{
    id: 1,
    title: 'Post 1',
    body: 'body 1',
    published: true
}, {
    id: 2,
    title: 'Post 2',
    body: 'body 2',
    published: false
}, {
    id: 3,
    title: 'Post 3',
    body: 'body 3',
    published: true
}]

// Demo user data
const users = [{
    id: '1',
    name: 'Felis',
    email: 'Felis@example.com',
    age: 27
}, {
    id: '2',
    name: 'Demo',
    email: 'test@example.com'
}, {
    id: '3',
    name: 'Mike',
    email: 'mike@hotmail.com',
    age: 22
}]

// Type definitions (schema)
const typeDefs = `
    type Query {
        greeting(name: String, position: String): String!
        add(numbers: [Float]): Float!
        grades: [Int!]!
        posts(query: String): [Post]!
        users(query: String): [User]!
        me: User!
        post: Post!
    }

    type User {
        id: ID!
        name: String!
        email: String!
        age: Int
    }

    type Post {
        id: ID!
        title: String!
        body: String!
        published: Boolean!
    }
`

// Resolvers
const resolvers = {
    Query: {
        greeting(parent, args, ctx, info) {
            if (args.name && args.position) {
                return `Hello! ${args.name}! You are my favorite ${args.position}.`
            }
            return 'Hello!'
        },
        grades(parent, args, ctx, info) {
            return [92, 81, 84, 22, 54]
        },
        posts(parent, args, ctx, info) {
            if (!args.query) {
                return posts
            }
            return posts.filter( (post) => {
                let titleMatch = post.title.toLowerCase().includes(args.query.toLowerCase())
                let bodyMatch = post.body.toLowerCase().includes(args.query.toLowerCase())
                return titleMatch || bodyMatch
                // return post.title.toLowerCase().includes(args.query) || post.body.toLowerCase().includes(args.query)
            })
        },
        users(parent, args, ctx, info) {
            if (!args.query) {
                return users
            }
            return users.filter((user) => {
                return user.name.toLowerCase().includes(args.query)
            })
        },
        me() {
            return {
                id: '123',
                name: 'Mike',
                email: 'mike@example.com',
                age: 25
            }
        },
        post() {
            return {
                id: '1234',
                title: 'Post',
                body: 'post body',
                published: false
            }
        },
        add(parent, args, ctx, info) {
            if (args.numbers.length === 0) {
                return 0
            }
            return args.numbers.reduce((accumulator, currentValue) => {
                return accumulator + currentValue
            })
        }
    }
}

const server = new GraphQLServer({
    typeDefs,
    resolvers
})

server.start(() => {
    console.log('The server is up.')
})
