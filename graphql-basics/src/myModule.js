// Name export - Has a name. Have as many as needed.
// Default export - Has no name. You can only have one.

const message = 'Some message from myModule.js'
const name = 'Felis'
const location = 'Thailand'

const getGreeting = (name) => {
    return `Hello, ${name}!`
}

console.log('Hello GraphQL')

export { message, name, getGreeting, location as default }